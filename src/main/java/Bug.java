
public class Bug 
{
		public String id;
		public String userG;
		public String genType;
		public String des;
		public String crit;
		public String opener;
		public String email;
		public String phone;
		public String first;
		public String last;
		public String summary;
		public boolean closed;
		
		
		public Bug(int id, String userG, String genType, String des, String crit, String opener, String email, String phone, String first, String last, String summary)
		{
			this.id = "bug" + id;
			this.userG = userG;
			this.genType = genType;
			this.des = des;
			this.crit = crit;
			this.opener = opener;
			this.email = email;
			this.phone = phone;
			this.first = first;
			this.last = last;
			this.closed = false;
			this.summary = summary;
		}
	
		public void close()
		{
			this.closed = true;
		}
		
		public boolean check(String in )
		{
			if(id.equals(in))
			{
				return true;
			}
			return false;
		}
	
	
	
	
}
