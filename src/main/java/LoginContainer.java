import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class LoginContainer 
{
	public ArrayList<logins> holder;
	
	public void LoginContainer() throws IOException
	{
		holder = new ArrayList<logins>(10);
		addFromCSV();
	}
	
	public  ArrayList<logins> addFromCSV() throws IOException
	{
		  BufferedReader CSVFile = new BufferedReader(new FileReader("logins"));
		  holder = new ArrayList<logins>(10);
		  CSVFile.readLine();
		  String dataRow = CSVFile.readLine();
			  while (dataRow != null)
			  {
				  logins pl = new logins(dataRow);
				  holder.add(pl);
				  dataRow = CSVFile.readLine(); // Read next line of data.
			  }
		   CSVFile.close();
		   System.out.println("Records added:" + holder.size());
		   return holder;
	}

	public ArrayList<logins> getList()
	{
		return holder;
	}
	public logins verify(String username, String password)
	{
		for(int x = 0; x < holder.size(); x++)
		{
			if(holder.get(x).equals(username, password) == true)
					return holder.get(x);
		}
		
		return null;
	}
	
	public void add(logins in)
	{
		holder.add(in);
	}
	
}
