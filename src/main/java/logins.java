import java.util.ArrayList;
import java.util.StringTokenizer;


public class logins 
{
		ArrayList<Bug> myBugs;
		public Person file;
		public String username;
		public String password;
		
		private String Fname;
		private String Lname;
		private String email;
		private String phone;
		private String provider;
		
	
	
		public logins(String dataRow) {
			myBugs = new ArrayList<Bug>();
			StringTokenizer st = new StringTokenizer(dataRow, ",");
			st.nextToken();
			username = st.nextToken();
			password = st.nextToken();
			Fname = st.nextToken();
			Lname = st.nextToken();
			email = st.nextToken();
			phone = st.nextToken();
			provider = st.nextToken();
		}

		public logins(String username, String Password, String Fname, String Lname, String email,String phone, String provider)
		{
			myBugs = new ArrayList<Bug>();
			this.username = username;
			this.password = Password;
			this.Fname = Fname;
			this.Lname = Lname;
			this.email = email;
			this.phone = phone;
			this.provider = provider;
		}
		
		public int addBug(Bug in)
		{
			for(int x = 0; x < myBugs.size(); x++)
			{
				if(myBugs.get(x).id.equals(in.id))
				{
					return 0;
				}
			}
			
			myBugs.add(in);
			return 1;
			
		}
		public ArrayList<Bug> bugs()
		{
			return myBugs;
		}
		
		public boolean equals(String username, String password)
		{
			if(this.username.equals(username) && this.password.equals(password))
				return true;
			else return false;
		}
		
		public Person getInfo()
		{
			return this.file;
		}
		
		public void attachPerson(Person in)
		{
			this.file = in;
		}
		
		
}
