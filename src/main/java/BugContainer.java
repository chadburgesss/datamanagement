import java.util.ArrayList;


public class BugContainer 
{
		ArrayList<Bug> conty;
		Bug temp;
		
	public BugContainer()
	{
		conty = new ArrayList<Bug>(10);
		
		
	}
	
	public void add(Bug in)
	{
		conty.add(in);
	}
		
	public int remove(Bug in)
	{
		for(int x = 0; x < conty.size(); x++)
		{
			if(conty.get(x).id.equals(in.id))
			{
				conty.remove(x);
				return 1;
			}
		}
		return 0;
		
	}
	
	public int modify(String id, String userG, String genType, String des, String crit, String email, String phone, String first, String last)
	{
		Bug hold;
		for(int x = 0; x < conty.size(); x++)
		{
			if(conty.get(x).id.equals(id))
			{
				hold = conty.get(x);
				hold.id = id;
				hold.userG = userG;
				hold.genType = userG;
				hold.des = des;
				hold.crit = crit;
				hold.email = email;
				hold.phone = phone;
				hold.first = first;
				hold.last = last;
				return 1;
			}
		}
		 return 0;

		
	}
	
}
