import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;


public class OneWindowToRuleThemAll {

	protected Shell shell;
	//protected Shell shellz;
	private Text text;
	private Text text_1;
	private Text text_2;
	private Text text_3;
	private String fName;
	private String num;
	private int number;
	private	String email;
	private String lName;
	private String provider;
	private Text desText;
	private Text text_6;
	private Text text_7;
	private Text Username;
	private Text txtDevasdad;
	private Text Password;
	private LoginContainer users = new LoginContainer();
	private int count = 1;
	private Text text_4;
	private Text text_10;
	private Text text_17;
	private Text text_18;
	private Text text_19;
	private BugContainer bugs = new BugContainer();
	private Bug tempB;
	

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			OneWindowToRuleThemAll window = new OneWindowToRuleThemAll();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		//shellz.open();
		//shellz.open();
		while (!shell.isDisposed() /*&& !shellz.isDisposed()*/) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(727, 498);
		shell.setText("SWT Application");
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		
		//shellz = new Shell();
		//shellz.setSize(600, 300);
		//shellz.setText("Shell2");
		//shellz.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		
		
		
		TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		
		TabItem tbtmNewItem = new TabItem(tabFolder, SWT.NONE);
		tbtmNewItem.setText("User Info");
		
		Composite composite = new Composite(tabFolder, SWT.NONE);
		tbtmNewItem.setControl(composite);
		composite.setLayout(null);
		
		text = new Text(composite, SWT.BORDER);
		text.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				fName = text.getText();
			}
		});
		text.setBounds(22, 75, 203, 22);
		
		Label lblFirstName = new Label(composite, SWT.NONE);
		lblFirstName.setBounds(22, 49, 67, 15);
		lblFirstName.setText("First Name");
		
		Label lblLastName = new Label(composite, SWT.NONE);
		lblLastName.setText("Last Name");
		lblLastName.setBounds(22, 103, 67, 23);
		
		text_1 = new Text(composite, SWT.BORDER);
		text_1.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				lName = text_1.getText();
			}
		});
		text_1.setBounds(22, 132, 203, 23);
		
		Label lblEmail = new Label(composite, SWT.NONE);
		lblEmail.setText("Email Adress");
		lblEmail.setBounds(22, 161, 79, 23);
		
		text_2 = new Text(composite, SWT.BORDER);
		text_2.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				email = text_2.getText();
			}
		});
		text_2.setBounds(22, 190, 331, 23);
		
		Label lblPhoneNumber = new Label(composite, SWT.NONE);
		lblPhoneNumber.setText("Cell Number");
		lblPhoneNumber.setBounds(22, 219, 118, 30);
		text_3 = new Text(composite, SWT.BORDER);
		text_3.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				num = text_3.getText();
				
			}
		});
		text_3.setBounds(22, 255, 165, 23);
		
		final Combo combo = new Combo(composite, SWT.READ_ONLY);
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				provider = combo.getText();
			}
		});
		combo.setBounds(22, 323, 118, 23);
		combo.add("Verizon");
		combo.add("Tmobile");
		combo.add("At&t");
		combo.add("Sprint");
		
		Label lblCellProvider = new Label(composite, SWT.NONE);
		lblCellProvider.setBounds(22, 295, 94, 22);
		lblCellProvider.setText("Cell Provider");
		
		Label lblUsername = new Label(composite, SWT.NONE);
		lblUsername.setBounds(430, 49, 55, 15);
		lblUsername.setText("Username");
		
		Username = new Text(composite, SWT.BORDER);
		Username.setEditable(false);
		Username.setBounds(430, 75, 243, 21);
		
		Label PasswordT = new Label(composite, SWT.NONE);
		PasswordT.setText("Password");
		PasswordT.setBounds(430, 111, 55, 15);
		
		Password = new Text(composite, SWT.BORDER);
		Password.setEditable(false);
		Password.setBounds(430, 134, 243, 21);
		
		Button cont = new Button(composite, SWT.NONE);

		cont.setBounds(298, 383, 75, 25);
		cont.setText("log");
		
		final Button SingleUse = new Button(composite, SWT.RADIO);
		SingleUse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Username.setEditable(false);
				Password.setEditable(false);
			}
		});

		SingleUse.setSelection(true);
		SingleUse.setBounds(22, 20, 79, 16);
		SingleUse.setText("Single Use");
		
		final Button CreateLogin = new Button(composite, SWT.RADIO);
		CreateLogin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Username.setEditable(true);
				Password.setEditable(true);
			}
		});
		CreateLogin.setBounds(108, 19, 90, 16);
		CreateLogin.setText("Create Login");
		
		final Button retuser = new Button(composite, SWT.RADIO);
		retuser.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Username.setEditable(true);
				Password.setEditable(true);
			}
		});
		retuser.setBounds(217, 20, 124, 16);
		retuser.setText("Returning User");
		
		TabItem tbtmCreate = new TabItem(tabFolder, SWT.NONE);
		tbtmCreate.setText("Create");
		
		Composite comp3 = new Composite(tabFolder, SWT.NONE);
		tbtmCreate.setControl(comp3);
		
		Label lblDescription_1 = new Label(comp3, SWT.NONE);
		lblDescription_1.setBounds(10, 10, 75, 15);
		lblDescription_1.setText("Description");
		
		desText = new Text(comp3, SWT.BORDER);
		desText.setBounds(10, 31, 683, 195);
		
		final Button critHi = new Button(comp3, SWT.RADIO);
		critHi.setBounds(523, 264, 90, 16);
		critHi.setText("High");
		
		final Button critMed = new Button(comp3, SWT.RADIO);
		critMed.setText("Medium");
		critMed.setBounds(523, 286, 90, 16);
		
		final Button critLow = new Button(comp3, SWT.RADIO);
		critLow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			}
		});
		critLow.setText("Low");
		critLow.setBounds(523, 308, 90, 16);
		
		Label lblCriticality_1 = new Label(comp3, SWT.NONE);
		lblCriticality_1.setBounds(523, 248, 55, 15);
		lblCriticality_1.setText("criticality");
		
		Label lblInclude = new Label(comp3, SWT.NONE);
		lblInclude.setBounds(10, 248, 55, 15);
		lblInclude.setText("Include");
		
		ScrolledComposite scrolledComposite_1 = new ScrolledComposite(comp3, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_1.setBounds(158, 248, 165, 174);
		scrolledComposite_1.setExpandHorizontal(true);
		scrolledComposite_1.setExpandVertical(true);
		
		final List listType = new List(scrolledComposite_1, SWT.BORDER);
		listType.add("Hardware Failure");
		listType.add("Memory Fault");
		listType.add("Internet Connection Failure");
		listType.add("Documentation error");
		listType.add("Logic error");
		listType.add("Infinite Loop");
		listType.add("Null Pointer Error");
		listType.add("Segmentation Fault");
		listType.add("General Resource Error");
		listType.add("Cannot Aquire Lock");
		listType.add("Access Violation");
		listType.add("Buffer Overflow");
		listType.add("Out of Bounds");
		listType.add("SQL error");
		listType.add("Injection Error");
		listType.add("File not found");
		listType.add("DOES NOT COMPUTE");
		listType.add("ERROR ERROR ERROR!");
		listType.add("Security Vulnerability");
		listType.add("Not performing to spec");
		listType.setSelection(1);
		scrolledComposite_1.setContent(listType);
		scrolledComposite_1.setMinSize(listType.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		Label lableistType = new Label(comp3, SWT.NONE);
		lableistType.setBounds(158, 232, 100, 15);
		lableistType.setText("General Type");
		
		ScrolledComposite scrolledComposite_2 = new ScrolledComposite(comp3, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_2.setBounds(339, 248, 165, 172);
		scrolledComposite_2.setExpandHorizontal(true);
		scrolledComposite_2.setExpandVertical(true);
		
		
		final List listGroup = new List(scrolledComposite_2, SWT.BORDER);
		listGroup.add("Management");
		listGroup.add("Administrators");
		listGroup.add("developers");
		listGroup.add("Tier 3");
		listGroup.add("Tier 2");
		listGroup.add("Tier 1");
		listGroup.setSelection(1);
		scrolledComposite_2.setContent(listGroup);
		scrolledComposite_2.setMinSize(listGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		
		Label lblUserGroup = new Label(comp3, SWT.NONE);
		lblUserGroup.setBounds(338, 230, 114, 15);
		lblUserGroup.setText("User Group");
		
		Button btnSubmit = new Button(comp3, SWT.NONE);

		btnSubmit.setBounds(601, 397, 75, 25);
		btnSubmit.setText("Create");
		
		final Button boxEmail = new Button(comp3, SWT.CHECK);
		boxEmail.setBounds(10, 264, 93, 16);
		boxEmail.setText("Email");
		
		final Button boxPhone = new Button(comp3, SWT.CHECK);
		boxPhone.setBounds(10, 286, 93, 16);
		boxPhone.setText("phone#");
		
		final Button boxName = new Button(comp3, SWT.CHECK);
		boxName.setBounds(10, 308, 93, 16);
		boxName.setText("name");
		
		TabItem tbtmNewItem_1 = new TabItem(tabFolder, SWT.NONE);
		tbtmNewItem_1.setText("Known Bugs");
		
		Composite composite_5 = new Composite(tabFolder, SWT.NONE);
		tbtmNewItem_1.setControl(composite_5);
		
		ScrolledComposite scrolledComposite_3 = new ScrolledComposite(composite_5, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_3.setBounds(10, 10, 142, 412);
		scrolledComposite_3.setExpandHorizontal(true);
		scrolledComposite_3.setExpandVertical(true);
		
		List list_3 = new List(scrolledComposite_3, SWT.BORDER);
		scrolledComposite_3.setContent(list_3);
		scrolledComposite_3.setMinSize(list_3.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		text_6 = new Text(composite_5, SWT.BORDER);
		text_6.setBounds(167, 27, 318, 68);
		
		text_7 = new Text(composite_5, SWT.BORDER);
		text_7.setEditable(false);
		text_7.setBounds(167, 312, 395, 53);
		
		Button btnNewButton_2 = new Button(composite_5, SWT.NONE);
		btnNewButton_2.setBounds(602, 397, 91, 25);
		btnNewButton_2.setText("modify");
		
		Label lblDescription = new Label(composite_5, SWT.NONE);
		lblDescription.setBounds(166, 10, 69, 15);
		lblDescription.setText("description");
		
		Label lblGenType = new Label(composite_5, SWT.NONE);
		lblGenType.setBounds(166, 101, 55, 15);
		lblGenType.setText("Gen Type");
		
		text_4 = new Text(composite_5, SWT.BORDER);
		text_4.setBounds(167, 122, 218, 21);
		
		Label lblUserGroup_1 = new Label(composite_5, SWT.NONE);
		lblUserGroup_1.setBounds(167, 149, 68, 15);
		lblUserGroup_1.setText("User Group");
		
		text_10 = new Text(composite_5, SWT.BORDER);
		text_10.setBounds(167, 170, 218, 21);
		
		Label lblNewLabel = new Label(composite_5, SWT.NONE);
		lblNewLabel.setBounds(166, 289, 142, 15);
		lblNewLabel.setText("closing sumary");
		
		Label label = new Label(composite_5, SWT.NONE);
		label.setText("criticality");
		label.setBounds(411, 111, 55, 15);
		
		Button button_2 = new Button(composite_5, SWT.RADIO);
		button_2.setText("Low");
		button_2.setBounds(411, 171, 90, 16);
		
		Button button_3 = new Button(composite_5, SWT.RADIO);
		button_3.setText("High");
		button_3.setBounds(411, 127, 90, 16);
		
		Button button_4 = new Button(composite_5, SWT.RADIO);
		button_4.setText("Medium");
		button_4.setBounds(411, 149, 90, 16);
		
		Button btnRadioButton = new Button(composite_5, SWT.RADIO);
		btnRadioButton.setBounds(472, 371, 90, 16);
		btnRadioButton.setText("Resolved?");
		
		Button btnNewButton = new Button(composite_5, SWT.NONE);
		btnNewButton.setBounds(354, 397, 208, 25);
		btnNewButton.setText("Remove Current");
		
		Label lblEmail_1 = new Label(composite_5, SWT.NONE);
		lblEmail_1.setBounds(167, 197, 55, 15);
		lblEmail_1.setText("email");
		
		text_17 = new Text(composite_5, SWT.BORDER);
		text_17.setBounds(167, 218, 218, 21);
		
		text_18 = new Text(composite_5, SWT.BORDER);
		text_18.setBounds(167, 262, 218, 21);
		
		Label lblNewLabel_3 = new Label(composite_5, SWT.NONE);
		lblNewLabel_3.setBounds(167, 245, 96, 15);
		lblNewLabel_3.setText("phone");
		
		Label lblNewLabel_4 = new Label(composite_5, SWT.NONE);
		lblNewLabel_4.setBounds(503, 27, 55, 15);
		lblNewLabel_4.setText("BUGID");
		
		text_19 = new Text(composite_5, SWT.BORDER);
		text_19.setEditable(false);
		text_19.setBounds(499, 48, 182, 21);
		
		Button btnLoadSelection = new Button(composite_5, SWT.NONE);
		btnLoadSelection.setBounds(160, 397, 142, 25);
		btnLoadSelection.setText("Load Selection");
		
		TabItem tbtmPreviousWork = new TabItem(tabFolder, SWT.NONE);
		tbtmPreviousWork.setText("Previous Work");
		
		Composite composite_6 = new Composite(tabFolder, SWT.NONE);
		tbtmPreviousWork.setControl(composite_6);
		
		Label lblNewLabel_5 = new Label(composite_6, SWT.NONE);
		lblNewLabel_5.setBounds(10, 20, 214, 15);
		lblNewLabel_5.setText("Bugs Associated With Account");
		
		ScrolledComposite scrolledComposite_6 = new ScrolledComposite(composite_6, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_6.setBounds(20, 41, 263, 381);
		scrolledComposite_6.setExpandHorizontal(true);
		scrolledComposite_6.setExpandVertical(true);
		
		List list_2 = new List(scrolledComposite_6, SWT.BORDER);
		scrolledComposite_6.setContent(list_2);
		scrolledComposite_6.setMinSize(list_2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("New Item");
		
		Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		tabItem_1.setControl(composite_1);
		
		//code functionality------------------------------------
		
			//tab2 button listner
		
		btnSubmit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			
				// creat new bug,
				//tempB = new Bug(count++,desText.getText(),listGroup.getSelection()
				//testing for correctness
				// of info
				System.out.println(desText.getText());
				System.out.println(listGroup.getItem(listGroup.getSelectionIndex()));
				System.out.println(listType.getItem(listType.getSelectionIndex()));
				System.out.println(boxEmail.getSelection());// email box
				System.out.println(boxPhone.getSelection()); // phone box
				System.out.println(boxName.getSelection()); // phone box
				System.out.println("high" + critHi.getSelection()); //
				System.out.println("med" + critMed.getSelection()); //
				System.out.println("low" + critLow.getSelection()); //
				
			}
		});
		
		
		
		
		
		
		
		
		// tabFolder.getTabList()[1].setEnabled(false);
		 tabFolder.getTabList()[2].setEnabled(false);
		 tabFolder.getTabList()[3].setEnabled(false);
		 tabFolder.getTabList()[4].setEnabled(false);
		
		
		
		
		txtDevasdad = new Text(composite_1, SWT.BORDER);
		//txtDevasdad.setText("This Application is for bug reporting\r\n");
		txtDevasdad.append("A user may either choose to be a one time\n " +
				"user or may create a login and be able to relogin \n" +
				"in the future. Some Accounts have additional \n" +
				"functionality such as viewing user information and \n" +
				"editing groups. If you create a user account \n" +
				"you will gain access to creating and saving custom bugs. \n" +
				"this is verson 1.0");
		txtDevasdad.setEditable(false);
		txtDevasdad.setBounds(69, 96, 304, 291);

		cont.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(SingleUse.getSelection())
				{
					
				}
				else if(CreateLogin.getSelection()) // this needs to create a login
				{
					
					//logins temp = logins
					
				}
				if(retuser.getSelection())
				{
					
			
				}
				
				
			}
		});
	}
}
